#[derive(Deserialize)]
pub struct User {
    username: String,
    // Determines whether this is a guest login or not
    password: Option<String>
}
