
mod user;

use user::User;

use rocket::response::status::BadRequest;
use rocket_contrib::json::Json;

use crate::error::{AuthError,JoinError};
use super::{RoomID,SubscriptionID};
use crate::message::Message;
// Hmm should this be plaintext
// or JSON
#[post("/login", format = "application/json", data = "<user>")]
pub fn login(user: Json<User>) -> String {
    unimplemented!()
}

//TODO: I don't know if I should also provide a GET endpoint for this

// Returns a JoinError
#[post("/join/<room_id>/as/<token>", data="<password>")]
pub fn join(room_id: u64, token: u64, password: Option<String>) -> Result<(),BadRequest<JoinError>> {
    unimplemented!()
}

// Returns a RoomID
#[get("/create_room")]
pub fn get_create_room() -> RoomID {
    unimplemented!()

}

// Returns a RoomID
#[post("/create_room", data="<password>")]
pub fn post_create_room(password: Option<String>) -> RoomID {
    unimplemented!()

}

// This takes a message and doesn't return anything
#[post("/lobby/send_message/as/<token>", data="<message>")]
pub fn send_message(message: String, token: u64) -> Result<(),BadRequest<AuthError>> {
    unimplemented!()
}

#[get("/lobby/messages")]
pub fn get_messages() -> Json<Vec<Message>> {
    unimplemented!()
}

// Returns a SubscriptionID
#[get("/lobby/messages/subscribe")]
pub fn subscribe() -> SubscriptionID {
    unimplemented!()
}

// Get all new messages in the subscription
#[get("/lobby/new_messages/<subscription_id>")]
pub fn get_new_messages(subscription_id: u64) -> Json<Vec<Message>>{
    unimplemented!()
}

