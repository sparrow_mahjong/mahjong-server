#[derive(Deserialize,Serialize)]
pub struct Message {
    user: String,
    time: String,
    contents: String,
}
