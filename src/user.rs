#[derive(Deserialize)]
pub struct User {
    name: String,
    password: Option<String>
}
