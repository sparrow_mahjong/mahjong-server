mod event;
mod state;

use rocket::response::status::BadRequest;
use rocket_contrib::json::Json;

use crate::error::{AuthError,Error};
use super::SubscriptionID;
use event::{Event,Action};
use state::GameState;

// And now onto the Game API

// Get the entire game state as a JSON object
#[get("/game/<room_id>/state/as/<token>")]
pub fn get_state(room_id: u64, token: u64) -> Result<Json<GameState>,BadRequest<Error>> {
    unimplemented!()
}

#[get("/game/<room_id>/subscribe/as/<token>")]
pub fn subscribe(room_id: u64, token: u64) -> Result<SubscriptionID,BadRequest<Error>>{
    unimplemented!()
}

// Get new events in the subscription
#[get("/game/<room_id>/new_events/<subscription_id>")]
pub fn get_new_events(room_id: u64, subscription_id: u64) -> Result<Json<Vec<Event>>,BadRequest<Error>> {
    unimplemented!()
}

// Submit a new action for the current user
#[post("/game/<room_id>/new_action/as/<token>", data="<action>")]
pub fn new_action(room_id: u64, action: Json<Action>, token: u64) -> Result<(),BadRequest<Error>> {
    unimplemented!()
}

// Helper functions to make it easier to take actions
#[get("/game/<room_id>/kong/as/<token>")]
pub fn kong(room_id: u64, token: u64) -> Result<(),BadRequest<Error>> {
    unimplemented!()
}

#[get("/game/<room_id>/pung/as/<token>")]
pub fn pung(room_id: u64, token: u64) -> Result<(),BadRequest<Error>> {
    unimplemented!()
}

#[get("/game/<room_id>/chow/with/<first_tile_id>/<second_tile_id>/as/<token>")]
pub fn chow(room_id: u64, token: u64, first_tile_id: u64, second_tile_id: u64) -> Result<(),BadRequest<Error>> {
    unimplemented!()
}

// Should this return the card drawn?
#[get("/game/<room_id>/draw/as/<token>")]
pub fn draw(room_id: u64, token: u64) -> Result<(),BadRequest<Error>> {
    unimplemented!()
}

#[get("/game/<room_id>/mahjong/as/<token>")]
pub fn mahjong(room_id: u64, token: u64) -> Result<(),BadRequest<Error>> {
    unimplemented!()
}

#[get("/game/<room_id>/discard/<tile_id>/as/<token>")]
pub fn discard(room_id: u64, token: u64, tile_id: u64) -> Result<(),BadRequest<Error>> {
    unimplemented!()
}

#[post("/game/<room_id>/message/as/<token>", data="<message>")]
pub fn message(room_id: u64, token: u64, message: String) {
    unimplemented!()
}
