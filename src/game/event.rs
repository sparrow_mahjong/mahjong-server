#[derive(Serialize)]
pub enum Event {
    
}

#[derive(Deserialize)]
pub enum Action {
    Pung,
    Kong,
    Chow(u64,u64),
    Discard(u64),
    Draw,
    Mahjong
}
