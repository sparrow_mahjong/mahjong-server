#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;
#[macro_use] extern crate serde_derive;

mod error;
mod game;
mod lobby;
mod message;
mod user;

use rocket_contrib::json::Json;
use rocket::response::status::BadRequest;
use user::User;


// These are both u64s formatted into a string
pub type RoomID = String;
pub type SubscriptionID = String;


fn main() {
    rocket::ignite()
        .mount("/", routes![
               lobby::login,
               lobby::join,
               lobby::get_create_room,
               lobby::post_create_room,
               lobby::send_message,
               lobby::get_messages,
               lobby::subscribe,
               lobby::get_new_messages,
               game::get_state,
               game::subscribe,
               game::get_new_events,
               game::new_action,
               game::kong,
               game::pung,
               game::chow,
               game::draw,
               game::mahjong,
               game::discard,
               game::message,
        ])
        .launch();
}
