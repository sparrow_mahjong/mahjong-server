use super::RoomID;

#[derive(Serialize,Deserialize,Debug)]
pub enum Error {
    AuthError(AuthError),
    InvalidRoom(RoomID),

}

#[derive(Serialize,Deserialize,Debug)]
pub enum AuthError {
    InvalidToken,
    FailedLogin
}


#[derive(Serialize,Deserialize,Debug)]
pub enum JoinError {
    InvalidRoom,
    InvalidPassword
}
